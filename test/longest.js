const assert = require('assert')
const longest = require('../lib/longest')

const CASE_ARRAY = [1,1,0,0,1,0,1,1,1,0,1,1,1];
const CASE_EMPTY_ARRAY = [];
const CASE_EMPTY_STRING = '';
const CASE_DIRTY = '1,2,1,3,1,0,1,1,1,1';
const CASE_TOO_SHORT = '1,0';
const CASE_ONE_ONLY = '1,1,1,1,1,1,1,1,1,1';
const CASE_ZERO_ONLY = '0,0,0,0,0,0,0,0,0,0';
const CASE_WITHOUT_BREAKERS_1 = '1,1,1,1,1,1,1,1,0';
const CASE_WITHOUT_BREAKERS_2 = '0,1,1,1,1,1,1,1,1,0';
const CASE_WITHOUT_BREAKERS_3 = '0,0,1,1,1,1,1,1,1,0';
const CASE_9 = '1,1,0,0,1,0,1,1,1,0,1,1,1';
const CASE_4 = '1,0,1,1,0,1,1,1,1';
const CASE_EQUAL_BREAKERS = '1,1,0,1,1,0,1,1';
const CASE_MORE_THAN_ONE_BREAKER = '1,1,0,1,1,1,0,1';
const CASE_TWO_BREAKERS_AND_NON_BREAKERS = '1,1,1,0,1,1,1,1,0,1,0,0,1';
const CASE_ARRAY_WITH_STRING = ['1',1,1,1];
const CASE_LONGEST_SIZE_4 = [1,0,1,1,0,1,1,1,1];

describe('Longest', () => {

  it('should accept array and return index 9 => CASE_ARRAY', () => {
    assert.equal(longest.find(CASE_ARRAY), 9);
  });

  it('should return index -1 => CASE_EMPTY_ARRAY', () => {
    assert.equal(longest.find(CASE_EMPTY_ARRAY), -1);
  });

  it('should return index -1 => CASE_EMPTY_STRING', () => {
    assert.equal(longest.find(CASE_EMPTY_STRING), -1);
  });

  it('should return index -1 => CASE_TOO_SHORT', () => {
    assert.equal(longest.find(CASE_TOO_SHORT), -1);
  });

  it('should return index -1 without sequence breaker => CASE_ONE_ONLY', () => {
    assert.equal(longest.find(CASE_ONE_ONLY), -1);
  });

  it('should return index -1 without sequence breaker => CASE_ZERO_ONLY', () => {
    assert.equal(longest.find(CASE_ZERO_ONLY), -1);
  });

  it('should return index -1 without sequence breaker => CASE_WITHOUT_BREAKERS_1', () => {
    assert.equal(longest.find(CASE_WITHOUT_BREAKERS_1), -1);
  });

  it('should return index -1 without sequence breaker => CASE_WITHOUT_BREAKERS_2', () => {
    assert.equal(longest.find(CASE_WITHOUT_BREAKERS_2), -1);
  });

  it('should return index -1 without sequence breaker => CASE_WITHOUT_BREAKERS_3', () => {
    assert.equal(longest.find(CASE_WITHOUT_BREAKERS_3), -1);
  });

  it('should return index 5 => CASE_DIRTY', () => {
    assert.equal(longest.find(CASE_DIRTY), 5);
  });

  it('should return index 9 => CASE_9', () => {
    assert.equal(longest.find(CASE_9), 9);
  });

  it('should return index 4 => CASE_4', () => {
    assert.equal(longest.find(CASE_4), 4);
  });

  it('should return index 2 (first index found) => CASE_EQUAL_BREAKERS', () => {
    assert.equal(longest.find(CASE_EQUAL_BREAKERS), 2);
  });

  it('should return index 2 => CASE_MORE_THAN_ONE_BREAKER', () => {
    assert.equal(longest.find(CASE_MORE_THAN_ONE_BREAKER), 2);
  });

})

describe('Longest utils', () => {

  it('should convert to array of numbers', () => {
    assert(longest.convertToArrayNumbers(CASE_ARRAY_WITH_STRING).every(i => typeof i === "number"))
  })

  it('should get longest sequence size', () => {
    assert.equal(longest.getLongestSequenceSize(CASE_LONGEST_SIZE_4), 4);
  })

  it('should replace a sequence breaker', () => {
    assert.deepEqual(longest.replaceSequenceBreaker(CASE_LONGEST_SIZE_4, 1), [1, 1, 1, 1, 0, 1, 1, 1, 1]);
  })

  it('should find all sequence breakers indexes', () => {
    assert.deepEqual(longest.findBreakers(CASE_4), [1, 4]);
  })

});