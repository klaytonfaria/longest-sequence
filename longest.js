#! /usr/bin/env node
const longest = require('./lib/longest');
let input = process.argv || '';
input = input.splice(2, Number.MAX_VALUE).join('');

console.log('index', longest.find(input));
