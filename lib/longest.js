const longest = (list) => {

  /**
   * Used to convert array items into numbers
   *
   * @method convertToArrayNumbers
   * @param {!(number[]|string)} list
   * @return number[]
   */

  function convertToArrayNumbers(list = []) {
    let listNumbers = list;
    if (typeof list === 'string') {
      listNumbers = list.split(',');
    }
    return listNumbers.map(Number);
  }

  /**
   * Used to get possible sequence breakers' indexes
   *
   * @method findBreakers
   * @param {!(number[]|string)} list
   * @param {number} [sequencer=1]
   * @param {number} [breaker=0]
   * @return number[]
   */

  function findBreakers(list = [], sequencer = 1, breaker = 0) {
    const numbers = convertToArrayNumbers(list);
    let result = [];
    numbers.map((el, i, arr) => {
      const nextItem = arr[i + 1];
      const prevItem = arr[i - 1];
      const isBreaker = nextItem === sequencer && prevItem === sequencer;
      if (el === breaker && isBreaker) {
        result.push(i);
      }
    });
    return result;
  };

  /**
  * Used to get the longest sequence in array.
  * When replace the sequence breaker (0) into a sequence item (1) this returns the longest sequence.
  * This is useful to measure the replace impact.
  *
  * @method getLongestSequenceSize
  * @param {!(number[]|string)} list
  * @param {!number} sequencer
  * @example getLongestSequenceSize(['1,1,1,0,0,1,1,1,1,0,1']); // => ['1,1,1,0,0,1,1,1,1,0,1']
  * @return number[]
  **/

  function getLongestSequenceSize(list = [], sequencer = 1) {
    const sequencePattern = new RegExp(`${sequencer}{2,}`, 'g');
    const sequenceList = list.join('').match(sequencePattern) || [];
    const sequenceSize = sequenceList.reduce((prev, current) => prev.length >= current.length ? prev : current).length;
    return sequenceSize;
  }

  /**
  * Used to replace a sequence breaker (0) to a sequence item (1)
  *
  * @method replaceSequenceBreaker
  * @param {!(number[]|string)} list
  * @param {!number} idx
  * @param {!number} sequencer
  * @example replaceSequenceBreaker(['1,0,1,0,0,1,1,1,1,0,1'], 1); // => ['1,1,1,0,0,1,1,1,1,0,1']
  * @return number[]
  **/

  function replaceSequenceBreaker(list = [], idx, sequencer = 1) {
    let sequenceList = [...list];
    if(typeof idx === 'number' && list.length > idx) {
      sequenceList[idx] = sequencer;
    }
    return sequenceList;
  }

  /**
  * Used to find the position of 0 on an array to be replaced with 1 to get longest continuous sequence of 1s.
  *
  * @method find
  * @param {!(number[]|string)} list
  * @example find('1,0,1,0,0,1,1,1,1,0,1');
  * @return number[]
  **/

  function find(list = [], sequencer = 1, breaker = 0) {
    const breakers = findBreakers(list, sequencer, breaker);
    let numberList = convertToArrayNumbers(list);
    let result = [];

    if (numberList.length < 3 || breakers === 0) {
      return -1;
    }

    breakers.map(i => {
      const size = getLongestSequenceSize(replaceSequenceBreaker(numberList, i));
      result.push({
        idx: i,
        size
      })
    });

    if (result.length === 0) {
      return -1;
    }

    return result.reduce((prev, val) => {
      return val.size > prev.size ? val : prev;
    }).idx
  }
  return {
    find,
    findBreakers,
    convertToArrayNumbers,
    getLongestSequenceSize,
    replaceSequenceBreaker
  }
}

module.exports = longest();
