# Longest
>Given an array of 0s and 1s, find the position of 0 to be replaced with 1 to get longest continuous sequence of 1s.

### Dependencies:

* node `>=v6.0.0`
* NPM `>=v3.0.0`

## Install
**install dependencies:**

```shell
npm install
```

**Link globally** to run without install as NPM global module

```shell
npm link
```

## Test

To run tests run:

```shell
npm test
```

## Running

### Usage
The program must run from the command line with the following arguments
```shell
Longest <sequence_of_0_or_1>
```

Example:

```shell
$ Longest 1,1,0,0,1,0,1,1,1,0,1,1,1
index 9
```

```shell
$ Longest 1,0,1,1,0,1,1,1,1
index 4
```